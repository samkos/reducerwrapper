////////////////////////////////////////////////////////////////////////////////////////////
// Slurm time machine browser
////////////////////////////////////////////////////////////////////////////////////////////
// Author: Samuel Kortas
// Copyright: 2018
////////////////////////////////////////////////////////////////////////////////////////////


import React, { Component } from 'react';

import { connect} from 'react-redux';

import Counter from './Counter';

class App extends Component {

	render() {
		return (
			<div>
			  <Counter nb={1} />
			  <br/>
			  <Counter nb={2} />				
			</div>
		);
	}
}

const mapStateToProps = ({red1, red2}) => ({
	c1: red1.counter,
	c2: red2.counter
})
	
const mapDispatchToProps = {
	//action1
}
	
export default connect(mapStateToProps, mapDispatchToProps)(App)
