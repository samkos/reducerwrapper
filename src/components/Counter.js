
import React, { Component } from 'react';

import { connect} from 'react-redux';

import { increase } from '../actions';

class Counter extends Component {

	render() {
		console.log("TCL: red", this.props.value )
		const val = this.props.value['red'+this.props.nb]
		return (
			<div>
				<center>
			  		<button onClick={  () => this.props.increase(this.props.nb) } >Counter #{ this.props.nb} &nbsp; { val }</button>
				</center>
		</div>
		);
	}
}

const mapStateToProps = (red) => ({
	value: red
})
	
const mapDispatchToProps = {
    increase
}
	
export default connect(mapStateToProps, mapDispatchToProps)(Counter)
