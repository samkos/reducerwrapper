import { combineReducers } from 'redux';

function counter(state = 0, action) {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1
    case 'DECREMENT':
      return state - 1
    default:
      return state
  }
}

function createNamedWrapperReducer(reducerFunction, reducerName) {
  return (state, action) => {
    const { name } = action
    const isInitializationCall = state === undefined
    console.log("TCL: createNamedWrapperReducer -> name, action, reducerName", name, action, reducerName)
    if (name !== reducerName && !isInitializationCall) {
      console.log('not calling reducer  name !== reducerName',name !== reducerName,"!isInitializationCall",!isInitializationCall)
      return state
    }
    console.log('calling reducer', reducerName)
    return reducerFunction(state, action)
  }
}

export default combineReducers({
  red1: createNamedWrapperReducer(counter, '1'),
  red2: createNamedWrapperReducer(counter, '2'),
})

